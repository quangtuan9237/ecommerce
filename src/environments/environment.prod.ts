export const environment = {
  production: true,
  apiUrl: 'http://storj-dota2divine.ddns.net',
  tokenKey: 'token',
  websocketUrl: 'ws://storj-dota2divine.ddns.net/chat',
};
