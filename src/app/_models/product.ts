import { ProductHttp } from './http-models';
import { environment } from 'environments/environment';

export class Product {
    _id: string;
    title: string;
    price: number;
    category: string;
    imageUrl: string;

    constructor(private product: ProductHttp) {
        this._id = this.product._id;
        this.title = this.product.title;
        this.price = this.product.price;
        this.category = this.product.category;
        this.imageUrl = environment.apiUrl + this.product.imageUrl;
    }
}