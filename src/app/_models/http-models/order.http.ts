import { ShoppingCartItemHttp } from './shopping-cart-item.http';

export interface OrderHttp {
    _id: string,
    user: string,
    items: ShoppingCartItemHttp[],
    createdDate: Date,
    shipping: shippingHttp,
}

export interface shippingHttp {
    name: string,
    address: string,
    address2: string,
    city: string,
}