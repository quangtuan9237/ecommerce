export interface ProductHttp {
    _id: string;
    title: string;
    price: number;
    category: string;
    imageUrl: string;
}
