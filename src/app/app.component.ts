import { Component } from '@angular/core';
import { WebsocketService, AuthenticationService } from './_services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  {
  title = 'e-commerce';

  constructor(
    private webSocket: WebsocketService,
    private authenticationService: AuthenticationService

  ) {
    this.authenticationService.currentUser.subscribe(
      (user) => {
        if (user) {
          this.webSocket.initSubject();
        } else {
          if(this.webSocket.subject){
            this.webSocket.subject.complete();
          }
        }
      }
    );
  }
}
