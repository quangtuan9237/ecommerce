import { Component, OnDestroy, AfterViewChecked, ViewChild, ElementRef, Input } from '@angular/core';
import { ChatService, iUploadFile, IN_EVT } from '@/_services';
import { DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { UploadFileResponse } from '../models/UploadFile';

@Component({
  selector: 'app-chat-box',
  templateUrl: './chat-box.component.html',
  styleUrls: ['./chat-box.component.scss']
})
export class ChatBoxComponent implements OnDestroy, AfterViewChecked {
  subscription: Subscription;
  subscription2: Subscription;
  subscription3: Subscription;
  subscription4: Subscription;

  messages = {};

  @Input('selected-friend') selectedFriend;

  @ViewChild("messageBox") el: ElementRef
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;

  constructor(
    private chatService: ChatService,
    private sanitizer: DomSanitizer,
    private toastr: ToastrService,
  ) { 

    this.subscription = this.chatService.subjects[IN_EVT.NEW_MESSAGE].subscribe(
      (wsData: iUploadFile) => {
        this.messages[wsData.from] = this.messages[wsData.from] || []; 
        wsData.incomming = true;
        this.messages[wsData.from].push(wsData);
      }
    );

    this.subscription2 = this.chatService.subjects[IN_EVT.SEND_MESSAGE_SUCCESS].subscribe(
      (wsData: iUploadFile) => {
        this.messages[wsData.receiver] = this.messages[wsData.receiver] || [];
        wsData.incomming = false;
        this.messages[wsData.receiver].push(wsData);
      }
    );
    
    this.subscription3 = this.chatService.subjects[IN_EVT.UPLOAD_FILE].subscribe(
      (wsData: iUploadFile) => {
        this.messages[wsData.from] = this.messages[wsData.from] || [];
        wsData.incomming = true;
        let message = new UploadFileResponse(wsData,this.sanitizer);
        this.messages[wsData.from].push(message);
      }
    );

    this.subscription4 = this.chatService.subjects[IN_EVT.UPLOAD_FILE_SUCCESS].subscribe(
      (wsData: iUploadFile) => {
        this.messages[wsData.receiver] = this.messages[wsData.receiver] || [];
        wsData.incomming = false;
        let message = new UploadFileResponse(wsData,this.sanitizer);
        this.messages[wsData.receiver].push(message);
      }
    );

    this.scrollToBottom();
  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  scrollToBottom(): void {
    try {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch (err) { }
  }

  sendMessage(message: string) {
    this.chatService.send(message, this.selectedFriend);
    this.el.nativeElement.value = "";
  }

  async uploadFile(file: File){
    if(file){
      if(file.size > 26214400){
        this.toastr.error("Only allow file size < 25MB", "File too large");
      }else{
        await this.chatService.uploadFile(file, this.selectedFriend);
      }
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.subscription2.unsubscribe();
    this.subscription3.unsubscribe();
    this.subscription4.unsubscribe();
  }
}
