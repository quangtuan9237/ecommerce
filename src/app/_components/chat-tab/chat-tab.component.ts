import { AuthenticationService } from '@/_services';
import { Component, OnInit, OnDestroy, ViewChild, ElementRef, AfterViewChecked } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-chat-tab',
  templateUrl: './chat-tab.component.html',
  styleUrls: ['./chat-tab.component.scss']
})
export class ChatTabComponent implements OnInit, OnDestroy {
  subscription1: Subscription;
  selectedFriend: string = "";
  myID: string;

  constructor(
    private route: ActivatedRoute,
    private authService: AuthenticationService,
  ) {
  }

  ngOnInit() {
    this.myID = this.authService.currentUserValue._id;

    this.subscription1 = this.route.queryParamMap.subscribe(param => {
      this.selectedFriend = param.get('selected')
    })

  }

  ngOnDestroy() {
    this.subscription1.unsubscribe();
  }
}
