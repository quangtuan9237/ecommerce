import { iUploadFile } from '@/_services';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';

export class UploadFileRequest{
     constructor(private file: File, private targetID:string){
 
    }

    async getPayload(){
      let fileReader = new FileReader(); 
    
      let promise1 = new Promise(function(resolve, reject) {
        fileReader.onload = (evt) => {
          resolve(fileReader.result);
        }
      });

      fileReader.readAsDataURL(this.file);

      let fileReaderResult = await promise1;

      let payload = {
        receiver: this.targetID,
        name: this.file.name, 
        type: this.file.type,
        size: this.file.size, 
        data: fileReaderResult, 
        isFile: true,
      }
      return payload;
    }
}

export class UploadFileResponse implements iUploadFile{
    from: string;
    receiver: string;
    time_date: number;
    message: string;
    incomming: boolean;
    name: string;
    type: string;
    size: number;
    data: string | SafeUrl;
    isFile: boolean;

    constructor(
      private responseData: iUploadFile,
      private sanitizer: DomSanitizer
      ){
        this.from = this.responseData.from;
        this.time_date = this.responseData.time_date;
        this.receiver = responseData.receiver; 
        this.message = responseData.message || ""; 
        this.incomming = responseData.incomming; 
        this.name = responseData.name || ""; 
        this.type = responseData.type || ""; 
        this.size = responseData.size || 0; 

        let url = URL.createObjectURL(b64toBlob(responseData.data as string));
        this.data = this.sanitizer.bypassSecurityTrustUrl(url);  
        this.isFile = responseData.isFile || false; 
    }
}

function b64toBlob(dataURI) {
  var byteString = atob(dataURI.split(',')[1]);
  var ab = new ArrayBuffer(byteString.length);
  var ia = new Uint8Array(ab);

  for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
  }
  return new Blob([ab]);
}