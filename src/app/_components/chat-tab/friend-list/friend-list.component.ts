import { Component, OnInit, Input, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { ChatService, iFriend, IN_EVT } from '@/_services/chat.service';
import { Observable, Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CreateGroupModalComponent } from './create-group-modal/create-group-modal.component';

@Component({
  selector: 'app-friend-list',
  templateUrl: './friend-list.component.html',
  styleUrls: ['./friend-list.component.scss']
})
export class FriendListComponent implements OnInit, OnDestroy {
  @Input('selected-friend') selectedFriend;
  friendList$: Observable<any>;
  friendResult$: Observable<any>;
  groupResult$: Observable<any>;
  friends: iFriend[];
  onSearch: boolean;
  @ViewChild("friendSearch") el: ElementRef

  friendsStatus = {};

  typeSelected = true;
  subscription: Subscription;
  subscription2: Subscription;
  subscription3: Subscription;

  constructor(
    private chatService: ChatService,
    private modalService: NgbModal,
    private router: Router,
  ) {

  }

  ngOnInit() {
    this.chatService.getAllFriends();
    this.chatService.getAllGroups();
    this.friendList$ = this.chatService.subjects[IN_EVT.FRIEND_LIST];

    this.chatService.getAllStatus();
    this.subscription = this.chatService.subjects[IN_EVT.RESPONSE_STATUS].subscribe((data) => {
      this.friendsStatus[data.from] = data.status; // receive the response status from the get_status event
    });
    this.subscription2 = this.chatService.subjects[IN_EVT.GET_STATUS].subscribe(data =>{
      this.friendsStatus[data.from] = true; // receive get status reques from user who has the id data.from
    });

    this.friendResult$ = this.chatService.subjects[IN_EVT.FIND_FRIEND_RESULT];
    this.groupResult$ = this.chatService.subjects[IN_EVT.FIND_GROUP_RESULT];

    this.subscription3 = this.chatService.subjects[IN_EVT.FRIEND_LIST].subscribe((friends)=>{
      if(!this.selectedFriend){
        this.router.navigate(['/messages'], { queryParams: { selected: friends[0]._id } });
      }
    });

    // this.chatService.subjects[IN_EVT.GROUP_LIST].subscribe((groups)=>{
    //   console.log(groups);
    // });
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
    this.subscription2.unsubscribe();
    this.subscription3.unsubscribe();
  }
  
  addGroup() {
    const modalRef = this.modalService.open(CreateGroupModalComponent);
    modalRef.result.then((groupName) => {
      if (groupName) {
        this.chatService.addGroup(groupName);
      }
    }, (reason) => {
      // modal dismissed
    });
  }

  getAllStatus(id: string) {
    id = id.trim()
    if (id) {
      this.chatService.findFriend(id);
      this.onSearch = true;
    }
  }

  findFriend(id: string) {
    id = id.trim()
    if (id) {
      this.chatService.findFriend(id);
      this.onSearch = true;
    }
  }
  
  sreachCancel() {
    this.onSearch = false;
    this.el.nativeElement.value = "";
  }

  addFriend(id: string) {
    this.chatService.addFriend(id);
    this.sreachCancel();
  }

  removeFriend(id: string) {
    this.chatService.removeFriend(id);
  }

  findGroup(id: string) {
    id = id.trim()
    if (id) {
      this.chatService.findGroup(id);
      this.onSearch = true;
    }
  }

  joinGroup(id: string) {
    this.chatService.joinGroup(id);
    this.onSearch = false;
  }
}
