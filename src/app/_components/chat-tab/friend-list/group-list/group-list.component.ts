import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { ChatService, IN_EVT } from '@/_services';

@Component({
  selector: 'app-group-list',
  templateUrl: './group-list.component.html',
  styleUrls: ['./group-list.component.scss']
})
export class GroupListComponent implements OnInit {
  groupList$: Observable<any>;
  @Input('selected-friend') selectedFriend;

  constructor(
    private chatService: ChatService,

  ) {
    this.groupList$ = this.chatService.subjects[IN_EVT.GROUP_LIST];

   }
  
  removeGroup(id:string){
    this.chatService.removeGroup(id);
  }
  
  ngOnInit() {
  }

}
