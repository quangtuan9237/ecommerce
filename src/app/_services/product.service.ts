import { Product } from '@/_models';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) {

  }

  create(data) {
    return this.http.post<Product>(`${environment.apiUrl}/api/products`, data).pipe(
      map((product) => {
        return new Product(product);
      })
    );
  }
  getAll() {
    return this.http.get<Product[]>(`${environment.apiUrl}/api/products`).pipe(
      map((products) => {
        return products.map(p => {
          return new Product(p);
        })
      })
    );
  }
  get(id) {
    return this.http.get<Product>(`${environment.apiUrl}/api/products/${id}`).pipe(
      map((product) => {
        return new Product(product);
      })
    );
  }
  update(id, data) {
    return this.http.put<Product>(`${environment.apiUrl}/api/products/${id}`, data).pipe(
      map((product) => {
        return new Product(product);
      })
    );
  }
  delete(id) {
    return this.http.delete<Product>(`${environment.apiUrl}/api/products/${id}`);
  }
}
