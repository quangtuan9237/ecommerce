import { WebsocketService, iSocketData } from './websocket.service';
import { Injectable, OnDestroy } from '@angular/core';
import { tap } from 'rxjs/operators';
import { Subject, Subscription } from 'rxjs';
import { UploadFileRequest } from '@/_components/chat-tab/models/UploadFile';
import { SafeUrl } from '@angular/platform-browser';

export const IN_EVT = {
  NEW_MESSAGE : 'new_message',
  SEND_MESSAGE_SUCCESS : 'send_message_success',
  FRIEND_LIST : 'friend_list',
  FIND_FRIEND_RESULT : 'find_friend_result',
  UPDATE_STATUS : 'update_status',
  GET_STATUS : 'get_status',
  RESPONSE_STATUS : 'response_status',
  UPLOAD_FILE : 'upload_file',
  UPLOAD_FILE_SUCCESS : 'upload_file_success',
  GROUP_LIST : 'group_list',
  FIND_GROUP_RESULT : 'find_group_result',

}

export const OUT_EVT = {
  REGISTER_USER : 'register_user',
  SEND_MESSAGE : 'send_message',
  FIND_FRIEND : 'find_friend',
  ADD_FRIEND : 'add_friend',
  REMOVE_FRIEND : 'remove_friend',
  GET_FRIENDS : 'get_friends',
  GET_ALL_STATUS : 'get_all_status',
  RESPONSE_STATUS : 'response_status',
  UPLOAD_FILE : 'upload_file',
  NEW_GROUP : 'new_group',
  GET_ALL_GROUPS: 'get_all_groups',
  REMOVE_GROUP:'remove_group',
  FIND_GROUP:'find_group',
  JOIN_GROUP:'join_group',
}

@Injectable({
  providedIn: 'root'
})
export class ChatService implements OnDestroy {
  subjects: Subject<any>[] = [];
  subscription: Subscription;

  constructor(
    private webSocket: WebsocketService,
  ) {
    for (var eventType in IN_EVT) {
      this.subjects[IN_EVT[eventType]] = new Subject();
    }

    this.webSocket.subject.pipe(
      tap((wsData) => {
        this.subjects[wsData.event].next(wsData.data)
      })
    ).subscribe((dump) => { });

    this.webSocket.send(OUT_EVT.REGISTER_USER, {}); // User logged in. Websocket is online.

    this.subscription = this.subjects[IN_EVT.GET_STATUS].subscribe(data =>{
      this.webSocket.send(OUT_EVT.RESPONSE_STATUS, {receiver: data.from, status: true});
    });
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();

  }

  send(message: string, targetID: string) {
    message = message.trim();
    if (message != "") {
      this.webSocket.send(OUT_EVT.SEND_MESSAGE, { receiver: targetID, message: message });
    }
  }

  findFriend(id: string) {
    this.webSocket.send(OUT_EVT.FIND_FRIEND, { friend_id: id });
  }

  addFriend(id: string) {
    this.webSocket.send(OUT_EVT.ADD_FRIEND, { friend_id: id });
  }

  removeFriend(id: string) {
    this.webSocket.send(OUT_EVT.REMOVE_FRIEND, { friend_id: id });
  }

  getAllFriends() {
    this.webSocket.send(OUT_EVT.GET_FRIENDS, {});
  }

  getAllStatus() {
    this.webSocket.send(OUT_EVT.GET_ALL_STATUS, {});
  }

  async uploadFile(file: File, targetID: string){
    let temp = new UploadFileRequest(file,targetID);
    let payload = await temp.getPayload();
    this.webSocket.send(OUT_EVT.UPLOAD_FILE, payload); 
  }

  addGroup(name: string){
    this.webSocket.send(OUT_EVT.NEW_GROUP, {name: name});
  }

  getAllGroups(){
    this.webSocket.send(OUT_EVT.GET_ALL_GROUPS,{});
  }

  removeGroup(id: string) {
    this.webSocket.send(OUT_EVT.REMOVE_GROUP, { group_id: id });
  }

  findGroup(id: string) {
    this.webSocket.send(OUT_EVT.FIND_GROUP, { group_id: id });
  }

  joinGroup(id: string) {
    this.webSocket.send(OUT_EVT.JOIN_GROUP, { group_id: id });
  }
}

export interface iChatMessages extends iSocketData {
  receiver: string,
  message: string,
  incomming: boolean,
}

export interface iUploadFile extends iChatMessages {
  isFile: boolean,
  name: string,
  type: string,
  size: number,
  data: string | SafeUrl,
}

export interface iFriend {
  messages: string[],
  lastDateMessage: number,
  friendDate: number,
  userInfo: {
    _id: string,
    name: string
  },
}